package comp;

public class Processor {
    public String getName() {
        return name;
    }

    private String name;
    public Processor (String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return name;
    }
}
