package comp;

public class Computer {
    private Processor processor;
    private GraphicCard graphicCard;
    private PowerSupply powerSupply;

    public Computer (Processor processor, GraphicCard graphicCard, PowerSupply powerSupply) {
        this.processor = processor;
        this.graphicCard = graphicCard;
        this.powerSupply = powerSupply;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor=" + processor +
                ", graphicCard=" + graphicCard +
                ", powerSupply=" + powerSupply +
                '}';
    }
}
