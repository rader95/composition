package comp;

public class ComputerFactory {
    public static Computer createComputer (String processorName, String graphicCardName, String powerSupplyName) {
        Processor processor = new Processor(processorName);
        GraphicCard graphicCard = new GraphicCard(graphicCardName);
        PowerSupply powerSupply = new PowerSupply(powerSupplyName);
        Computer computer = new Computer(processor, graphicCard, powerSupply);
        return computer;
    }
}
